### Install Dependencies

RUN `npm install`

Behind the scenes this will also call `bower install`.  You should find that you have two new folders in your project.

* `node_modules` - contains the npm packages for the tools
* `app/bower_components` - contains the angular framework files

### Run the Application

RUN `npm start`

Now browse to the app at `http://localhost:8000/app/index.html`.