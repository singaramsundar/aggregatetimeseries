'use strict';

angular.module('myApp.main', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/main', {
            templateUrl: 'main/main.html',
            controller: 'MainCtrl'
        });
    }])

    .controller('MainCtrl', ['$scope', '$http', function ($scope, $http) {

        var url = 'data/inputData.csv';

        var options = {
            lines: {show: true},
            points: {show: true},
            axisLabels: {
                show: true
            },
            xaxis: {
                mode: "time",
                timeformat: "%y.%d.%m",
                axisLabel: "Date"
            },
            yaxes: [
                {
                    axisLabel: "Component",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                    axisLabelPadding: 5
                },
                {
                    position: 0,
                    alignTicksWithAxis: 1,
                    axisLabel: "Weight",
                    //labelPadding: 7
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                    axisLabelPadding: 5
                }
            ],
            grid: {
                hoverable: true,
                borderWidth: 1
            }
        };

        $http.get(url).success(function (data) {
            Papa.parse(data, {
                dynamicTyping: true,
                complete: function (results) {
                    var chartData = [];
                    var data1 = [];
                    var data2 = [];
                    var data3 = [];
                    var data4 = [];
                    var data5 = [];
                    var data6 = [];
                    var data7 = [];

                    for (var i = 0; i < results.data.length; i++) {
                        var date = new Date(results.data[i][0]).getTime();

                        data1.push([date, results.data[i][1]]);
                        data2.push([date, results.data[i][3]]);
                        data3.push([date, results.data[i][5]]);
                        data4.push([date, results.data[i][7]]);
                        data5.push([date, results.data[i][8]]);
                        data6.push([date, results.data[i][9]]);
                        data7.push([date, results.data[i][10]]);
                    }

                    $.plot("#placeholder", [{
                            label: "Component 1",
                            data: data1,
                            lines: {show: true}
                        }, {
                            label: "Component 2",
                            data: data2,
                            lines: {show: true}
                        }, {
                            label: "Component 3",
                            data: data3,
                            lines: {show: true}
                        }, {
                            label: "Normalized weight1",
                            data: data4,
                            lines: {show: true, fill: true},
                            yaxis: 2
                        }, {
                            label: "Normalized weight2",
                            data: data5,
                            lines: {show: true, fill: true},
                            yaxis: 2
                        }, {
                            label: "Normalized weight3",
                            data: data6,
                            lines: {show: true, fill: true},
                            yaxis: 2
                        }, {
                            label: "Aggregate",
                            data: data7,
                            lines: {show: true}
                        }],
                        options
                    );
                }
            });
        });
    }]);